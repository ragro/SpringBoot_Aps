package com.example.tute.spring.BootYoutube.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Topic {

	@Id
	private String name;
	private String subject;
	private String type;
	
	public Topic() {
		
	}
	
	public Topic(String name, String subject, String type) {
		super();
		this.name = name;
		this.subject = subject;
		this.type = type;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
}
