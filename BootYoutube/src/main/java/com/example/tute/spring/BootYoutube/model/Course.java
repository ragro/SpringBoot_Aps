package com.example.tute.spring.BootYoutube.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Course {

	@Id
	private String name;
	private String subject;
	private String type;
	
	@ManyToOne
	private Topic topic;
	
	public Course() {
		
	}
	
	public Course(String name, String subject, String type, String topicName) {
		super();
		this.name = name;
		this.subject = subject;
		this.type = type;
		this.topic = new Topic(topicName,"","");
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}
}
