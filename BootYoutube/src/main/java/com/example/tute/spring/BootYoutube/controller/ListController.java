package com.example.tute.spring.BootYoutube.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.tute.spring.BootYoutube.model.Topic;
import com.example.tute.spring.BootYoutube.service.TopicService;

@RestController
public class ListController {

	@Autowired
	private TopicService topicService;

	public ListController(@Autowired TopicService topicService) {
		super();
		this.topicService = topicService;
	}

	@RequestMapping("/topics")
	public List<Topic> getList(){
		return topicService.getAllTopic();		
	}
	
	@RequestMapping("/topics/{id}")
	public Optional<Topic> getByName(@PathVariable String id) { // if pathvariable is different only other wise no need to give name
//		System.out.println(id);
		return topicService.getByName(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/topics")
	public void addTopic(@RequestBody Topic topic) {
		topicService.addTopic(topic);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/topics/{id}")
	public void updateTopic(@RequestBody Topic topic,@PathVariable String id ) {
		topicService.updateTopic(topic, id);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/topics/{id}")
	public void deleteTopic(@PathVariable String id) {
		topicService.deleteByName(id);
	}
}
