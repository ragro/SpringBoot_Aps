package com.example.tute.spring.BootYoutube.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.tute.spring.BootYoutube.model.Course;

public interface CourseRepository extends CrudRepository<Course, String> {
	public List<Course> findByTopicName(String name);
}
