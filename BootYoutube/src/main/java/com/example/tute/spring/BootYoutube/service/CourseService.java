package com.example.tute.spring.BootYoutube.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.tute.spring.BootYoutube.model.Course;
import com.example.tute.spring.BootYoutube.model.Topic;
import com.example.tute.spring.BootYoutube.repository.CourseRepository;
import com.example.tute.spring.BootYoutube.repository.TopicRepository;

@Service
public class CourseService {
	
	@Autowired
	private CourseRepository courseRepository;

	//	private List<Topic> topics=new ArrayList(Arrays.asList(new Topic("RestfulAPI", "API Testing","Moderate"),
//				new Topic("NodeJs", "Server Side Scripting","Easy"),
//				new Topic("Spring", "Server Side Scripting","Moderate")));

	public CourseService() {
		
	}
	
	public List<Course> getAllCourses(String topicName) {
		List<Course> courses = new ArrayList<>();
	    courseRepository.findByTopicName(topicName).forEach(courses::add);	    
	    return courses;
	}
	
	public Optional<Course> getByName(String name) {
		return courseRepository.findById(name);
	}
	
	public void addCourse(Course course) {
		courseRepository.save(course);
	}

	public void updateCourse(Course course) {
//		for(int i=0; i<topics.size(); i++) {
//			Topic t = topics.get(i);s
//			if(t.getName().equals(id)) {
//				topics.set(i, topicValue);
//				return;
//			}
//		}
		courseRepository.save(course);
	}

	public void deleteByName(String id) {
//		for(Topic topic : topics) {
//			if(topic.getName().equals(id)) {
//			   topics.remove(topic);
//			}
//		}
		courseRepository.deleteById(id);
	}
}
