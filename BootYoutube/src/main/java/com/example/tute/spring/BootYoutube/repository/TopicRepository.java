package com.example.tute.spring.BootYoutube.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.tute.spring.BootYoutube.model.Topic;

public interface TopicRepository extends CrudRepository<Topic, String> {

}
