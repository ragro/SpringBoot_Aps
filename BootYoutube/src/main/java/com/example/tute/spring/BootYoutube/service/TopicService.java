package com.example.tute.spring.BootYoutube.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.tute.spring.BootYoutube.model.Topic;
import com.example.tute.spring.BootYoutube.repository.TopicRepository;

@Service
public class TopicService {
	
	@Autowired
	private TopicRepository topicRepository;

	//	private List<Topic> topics=new ArrayList(Arrays.asList(new Topic("RestfulAPI", "API Testing","Moderate"),
//				new Topic("NodeJs", "Server Side Scripting","Easy"),
//				new Topic("Spring", "Server Side Scripting","Moderate")));

	public TopicService() {
		
	}
	
	public List<Topic> getAllTopic() {
		List<Topic> topics = new ArrayList<>();
	    topicRepository.findAll().forEach(topics::add);	    
	    return topics;
	}
	
	public Optional<Topic> getByName(String name) {
		return topicRepository.findById(name);
	}
	
	public void addTopic(Topic topic) {
		topicRepository.save(topic);
	}

	public void updateTopic(Topic topicValue, String id) {
//		for(int i=0; i<topics.size(); i++) {
//			Topic t = topics.get(i);
//			if(t.getName().equals(id)) {
//				topics.set(i, topicValue);
//				return;
//			}
//		}
		topicRepository.save(topicValue);
	}

	public void deleteByName(String id) {
//		for(Topic topic : topics) {
//			if(topic.getName().equals(id)) {
//			   topics.remove(topic);
//			}
//		}
		topicRepository.deleteById(id);
	}
}
