package com.cfe.springBootDemo.DemoSpringboot;

public class Book {
	private String name;
	private String author;
	private long id;
	
	public Book() {
		
	}
	public Book( long id,String name, String author) {
		super();
		this.name = name;
		this.author = author;
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String toString() {
		return "Book Details :  Book : "+name+" Book_id : "+id+" Author : "+author;
	}
	
}
