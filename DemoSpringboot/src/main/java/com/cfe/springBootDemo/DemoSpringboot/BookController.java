package com.cfe.springBootDemo.DemoSpringboot;

import java.util.List;

import org.assertj.core.util.Arrays;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {
	    
	@GetMapping("/books")
	
	public Book getAllBooks(){
//			return Arrays.asList(new Book(1l,"Mastering Spring 5.0", "Rangakarna"));
		return new Book(1l,"Mastering Spring 5.0", "Rangakarna");
	}
	
	@GetMapping("/")
	
	public String index() {
		return "index";
	}
}
