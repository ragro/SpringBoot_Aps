//package hotel;
//
//import java.util.Arrays;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.stereotype.Service;
//
//import hotel.model.Address;
//import hotel.model.Hotel;
//import hotel.model.Review;
//import hotel.repo.HotelRepo;
//import hotel.repo.ReviewRepo;
//
//@Service
//public class SeedDb implements CommandLineRunner {
//
//	@Autowired
//	HotelRepo hotelrepo;
//	
//	@Autowired
//	ReviewRepo reviewrepo; 
//
//	@Override
//	public void run(String... args) throws Exception {
//		hotelrepo.deleteAll();
//		reviewrepo.deleteAll();
//		
//		Hotel h1 = new Hotel("Radisson", 12000, new Address("Faridabad","India"));
//
//		Review r1 =  new Review(1,"worse");
//		Review r2 = new Review(2,"better");
//		
//		h1.setReviews(Arrays.asList(r1, r2));
//		
//		System.out.println(h1.toString());
//		
//		hotelrepo.save(h1);
//		
//		//		Hotel h1 = new Hotel("Radisson", 120,null, null);
////		Review review1 = new Review(5, "Nice Food",null);
////		h1.setAddress( new Address("Faridabad", "India"));
////
////		
////		
////		h1.setReviews(Arrays.asList(review1));
////		hotelrepo.save(h1);
////		
////		review1.setHotel(h1);		
////		reviewrepo.save(review1);
//		
//		
////		Hotel h2 = new Hotel("Hayat", 190, new Address("Delhi", "India"),
////		           Arrays.asList(new Review(1, "Nice Room service"), new Review(2, "Beautifull garden")));
////		
////		Hotel h3 = new Hotel("Plaza", 420, new Address("Kolkata", "India"), 
////				Arrays.asList(new Review(1, "Great Service"), new Review(1, "Delicious Food")));
////		
////		System.out.println(h2.toString());
////
////		List<Hotel> hotels = Arrays.asList(h1, h2, h3);
////		hotelrepo.saveAll(hotels);
//
//	}
//
//}
