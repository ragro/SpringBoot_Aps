package hotel.converter;

import hotel.dto.ReviewDTO;
import hotel.model.Review;

public class ReviewConverter {
	
	public static Review dtoToEntity(ReviewDTO reviewDto) {
		Review review = new Review();
		review.setId(reviewDto.getId());
		review.setComment(reviewDto.getComment());
		review.setRating(reviewDto.getRating());
		review.setHotel(reviewDto.getHotel());
		
		return review;
	}
	
	public static ReviewDTO entiyToDto(Review review) {
		ReviewDTO reviewDto = new ReviewDTO();
		reviewDto.setId(review.getId());
		reviewDto.setComment(review.getComment());
		reviewDto.setRating(review.getRating());
		reviewDto.setHotel(review.getHotel());
		
		return reviewDto;
	}
}

