package hotel.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import hotel.dto.HotelDTO;
import hotel.dto.ReviewDTO;
import hotel.model.Hotel;
import hotel.model.Review;

public class HotelConverter {
	public static Hotel dtoToEntity(HotelDTO hotelDto) {
		Hotel hotel = new Hotel();
		hotel.setId(hotelDto.getId());
		hotel.setName(hotelDto.getName());
		hotel.setPrice(hotelDto.getPrice());
		hotel.setAddress(hotelDto.getAddress());
		
		hotel.setReviews(hotelDto.getReviews().stream().map(ReviewConverter :: dtoToEntity).collect(Collectors.toList()));
		
		return hotel;
	}
	
	public static HotelDTO entityToDto(Hotel hotel) {		
		HotelDTO hotelDto = new HotelDTO();
		hotelDto.setId(hotel.getId());
		hotelDto.setAddress(hotel.getAddress());
		hotelDto.setName(hotel.getName());
		hotelDto.setPrice(hotel.getPrice());
		
		List<Review> reviews = hotel.getReviews();
		List<ReviewDTO> reviewDto = new ArrayList<>();
		for(Review review : reviews) {
			reviewDto.add(ReviewConverter.entiyToDto(review));
		}
		hotelDto.setReviews(reviewDto);		
		
		return hotelDto;
	}
}
