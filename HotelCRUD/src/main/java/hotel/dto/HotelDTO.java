package hotel.dto;

import java.util.List;

import hotel.model.Address;
import hotel.model.Review;

public class HotelDTO {
	private long id;
	private String name;
	private int price;
	private Address address;
	private List<ReviewDTO> reviews;
	
	public HotelDTO() {
		super();
	}
	
	public HotelDTO(long id, String name, int price, Address address, List<ReviewDTO> reviews) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.address = address;
		this.reviews = reviews;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public List<ReviewDTO> getReviews() {
		return reviews;
	}
	public void setReviews(List<ReviewDTO> reviews) {
		this.reviews = reviews;
	}
	@Override
	public String toString() {
		return "HotelDTO [id=" + id + ", name=" + name + ", price=" + price + ", address=" + address + ", reviews="
				+ reviews + "]";
	}
	
	
}