package hotel.dto;


import hotel.model.Hotel;

public class ReviewDTO {
	private Long id;
	private int rating;
	private String comment;
	private Hotel hotel;
	
	public ReviewDTO() {
		super();
	}
	public ReviewDTO(Long id, int rating, String comment, Hotel hotel) {
		super();
		this.id = id;
		this.rating = rating;
		this.comment = comment;
		this.hotel = hotel;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Hotel getHotel() {
		return hotel;
	}
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	@Override
	public String toString() {
		return "ReviewDTO [id=" + id + ", rating=" + rating + ", comment=" + comment + ", hotel=" + hotel + "]";
	}	
}
