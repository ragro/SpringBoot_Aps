package hotel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;

@Entity
public class Review {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private int rating;
	private String comment;
	
	@ManyToOne
	private Hotel hotel;

	public Review() {
		super();
	}

	public Review(int rating, String comment) {
		super();
		this.rating = rating;
		this.comment = comment;
	}

	public Review(int rating, String comment, Hotel hotel) {
		super();
		this.rating = rating;
		this.comment = comment;
		this.hotel = hotel;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getRating() {
		return rating;
	}
	public String getComment() {
		return comment;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Hotel getHotel() {
		return hotel;
	}
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
//	@Override
//	public String toString() {
//		return "Review [id=" + id + ", rating=" + rating + ", comment=" + comment + ", hotel=" + hotel + "]";
//	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Review [id=");
		builder.append(id);
		builder.append(", rating=");
		builder.append(rating);
		builder.append(", comment=");
		builder.append(comment);
		builder.append(", hotel=");
		builder.append(hotel);
		builder.append("]");
		return builder.toString();
	}
	
	
}
