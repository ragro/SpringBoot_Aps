package hotel.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Hotel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private int price;

	@Embedded
	private Address address;
	
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "hotel")
	@Column(name = "data")
	private List<Review> reviews;
	
	public Hotel() {
		super();
	}

	
	public Hotel(String name, int price, Address address) {
		super();
		this.name = name;
		this.price = price;
		this.address = address;
	}


	public Hotel(String name, int price, Address address, List<Review> reviews) {
		super();
		this.name = name;
		this.price = price;
		this.address = address;
		this.reviews = reviews;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public int getPrice() {
		return price;
	}
	public Address getAddress() {
		return address;
	}
	public List<Review> getReviews() {
		return reviews;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	@Override
	public String toString() {
		return "Hotel [id=" + id + ", name=" + name + ", price=" + price + ", address=" + address + ", reviews="
				+ reviews + "]";
	}	
}
