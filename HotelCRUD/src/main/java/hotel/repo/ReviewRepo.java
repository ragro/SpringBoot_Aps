package hotel.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import hotel.model.Review;

public interface ReviewRepo extends JpaRepository<Review, Long> {

}
