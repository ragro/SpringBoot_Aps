package hotel.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hotel.model.Hotel;

//@Repository
//public interface HotelRepo extends MongoRepository<Hotel,String> {
//
//	public Hotel findByName(String id);
//	public List<Hotel> findByPriceLessThan(int price);
//	public String deleteByName(String name);
//}

@Repository
public interface HotelRepo extends JpaRepository<Hotel,Long> {
	public Hotel findById(long id);
	public List<Hotel> findByPriceLessThan(int price);
	public String deleteById(long id);
}
