package hotel.service;

import java.util.List;

import hotel.dto.HotelDTO;

public interface HotelService {
	public List<HotelDTO>getAllHotel();
	public HotelDTO getHotelById(long id);
	public List<HotelDTO>getHotelByPrice(int price);
	public String addHotel(HotelDTO hotelDto);
	public String updateHotel(HotelDTO hotelDto, long id);
	public String deleteHotel(long id);
}

