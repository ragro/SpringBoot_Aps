package hotel.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hotel.converter.HotelConverter;
import hotel.dto.HotelDTO;
import hotel.model.Hotel;
import hotel.repo.HotelRepo;

@Service
public class HotelServiceImpl implements HotelService {

	@Autowired
	HotelRepo hotelRepo;
	
	public HotelServiceImpl(HotelRepo hotelrepo) {
		super();
		this.hotelRepo = hotelrepo;
	}
	
	@Override
	public List<HotelDTO> getAllHotel() {
		List<Hotel> hotels = new ArrayList<>();
		hotelRepo.findAll().forEach(hotels::add);
		
		List<HotelDTO> hotelDtos = new ArrayList<>();
		for(Hotel hotel : hotels) {
			hotelDtos.add(HotelConverter.entityToDto(hotel));
		}
		return hotelDtos;
	}

	@Override
	public HotelDTO getHotelById(long id) {
		Hotel hotel = hotelRepo.findById(id);
		return HotelConverter.entityToDto(hotel);
	}

	@Override
	public List<HotelDTO> getHotelByPrice(int price) {
		List<Hotel> hotels = hotelRepo.findByPriceLessThan(price);
		List<HotelDTO> hotelDtos = new ArrayList<>();
		
		for(Hotel hotel : hotels) {
			hotelDtos.add(HotelConverter.entityToDto(hotel));
		}
		return hotelDtos;
	}

	@Override
	public String addHotel(HotelDTO hotelDto) {
		Hotel hotel = HotelConverter.dtoToEntity(hotelDto);
		String res=hotelRepo.save(hotel).toString();
		return res!=null?"success":"not added";		
	}

	@Override
	public String updateHotel(HotelDTO hotelDto, long id) {
		Hotel hotel = HotelConverter.dtoToEntity(hotelDto);
		hotel.setId(id);
		hotelRepo.save(hotel);
		return "updated";
		
	}

	@Override
	public String deleteHotel(long id) {
		if(hotelRepo.findById(id) != null) {
			hotelRepo.deleteById(id);
			return "Deleted";
			
		}else {
			return "Not Found";
		}		
	}
}
