package hotel.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hotel.dto.HotelDTO;
import hotel.model.Address;
import hotel.model.Hotel;
import hotel.model.Review;
import hotel.repo.HotelRepo;
import hotel.repo.ReviewRepo;
import hotel.service.HotelService;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/"})
public class HotelController {

	@Autowired
	HotelService hotelService;

	@Autowired
	HotelRepo hotelrepo;
	
	@Autowired
	ReviewRepo reviewrepo; 

	@GetMapping({ "/", "" })
	public String rootPage() {
		hotelrepo.deleteAll();
		reviewrepo.deleteAll();
		
		Hotel h1 = new Hotel("Radisson", 12000, new Address("Faridabad","India"));

		Review r1 =  new Review(1,"worse");
		Review r2 = new Review(2,"better");
		
		h1.setReviews(Arrays.asList(r1, r2));
		
		hotelrepo.save(h1);
		System.out.println(h1.toString());

		return "Welcome to Root Page";
	}
	
//	@GetMapping("/hotels")
//	public List<HotelDTO> getAllHotels() {
//		return hotelService.getAllHotel();
//	}
//	
//	@GetMapping("/hotel/{id}")
//	public HotelDTO getOneHotel(@PathVariable Long id) {
//		return hotelService.getHotelById(id);		
//	}
//	
//	@GetMapping("/hotel/price/{price}")
//	public List<HotelDTO> hotelLessThanPrice(@PathVariable("price") int price){
//		return hotelService.getHotelByPrice(price);
//	}
//	
//	@PostMapping("/hotel")
//	public String addHotel(@RequestBody HotelDTO hotelDto) {
//		return hotelService.addHotel(hotelDto);
//	}
//	
//	@PutMapping("/hotel/{id}")
//	public String updateHotel(@RequestBody HotelDTO hotelDto, @PathVariable long id) {
//		return hotelService.updateHotel(hotelDto, id);
//	}
//	
//	@DeleteMapping("/hotel/{id}")
//	public String deleteHotel(@PathVariable Long id ) {
//		return hotelService.deleteHotel(id);
//	}
}
