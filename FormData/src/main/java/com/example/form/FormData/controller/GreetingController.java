package com.example.form.FormData.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.form.FormData.model.Greeting;
import com.example.form.FormData.repository.GreetingRepository;

@Controller
public class GreetingController {
	
	@Autowired
	private GreetingRepository greetingRepository;
	
	public GreetingController(GreetingRepository greetingRepository) {
		super();
		this.greetingRepository = greetingRepository;
	}

	@GetMapping("/greeting")
	public String greetingForm(Model model) {
		model.addAttribute("greeting", new Greeting());
		return "greeting";
	}
	
	@PostMapping("/greeting")
	public String greetingSubmit(@ModelAttribute Greeting greeting) {		
		greetingRepository.save(greeting);
		return "greeting";
	}
	
	@GetMapping("/get")
	public String getGreeting(Model model) {
		List<Greeting> greetings = greetingRepository.findAll();
		model.addAttribute("data", greetings);		
		return "getGreeting";
	}

//	@Override
//	public void run(String... args) throws Exception {
//		
//		greetingRepository.save(new Greeting("Faridabad","Nivedita"));
//		greetingRepository.save(new Greeting("Chandigarh","Sachin"));
//		
//		for(Greeting greeting : greetingRepository.findAll()) {
//			System.out.println(greeting);
//		}
//	}    
}
  