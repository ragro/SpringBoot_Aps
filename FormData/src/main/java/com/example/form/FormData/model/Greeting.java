package com.example.form.FormData.model;

import org.springframework.data.annotation.Id;

public class Greeting {
	   
	@Id private String id;
	
	private String city;
	private String name;
	
	public Greeting() {
		
	}
	public Greeting(String city, String name) {
		super();
		this.city = city;
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
