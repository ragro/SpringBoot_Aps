package com.example.form.FormData.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.form.FormData.model.Greeting;

public interface GreetingRepository extends MongoRepository<Greeting, String> {
	public List<Greeting> findByCity(String city);
	public List<Greeting> findByName(String name);
}
